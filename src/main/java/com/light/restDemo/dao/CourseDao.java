package com.light.restDemo.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.light.restDemo.entities.Course;

public interface CourseDao extends JpaRepository<Course, Long> {
	
}
