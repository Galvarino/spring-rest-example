package com.light.restDemo.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.light.restDemo.entities.Course;
import com.light.restDemo.services.CourseService;

@RestController
public class MyController {
	@Autowired
	private CourseService courseService;

	@GetMapping("/home")
	public String home() {
		return "this is home page";
	}

	@GetMapping("/courses")
	public List<Course> getCourses() {
		return this.courseService.getCourses();
	}

	@GetMapping("/courses/{courseId}")
	public ResponseEntity<?> getCourse(@PathVariable String courseId) {
		Course result = this.courseService.getCourse(Long.parseLong(courseId));
		if (result == null) {
			return ResponseEntity.status(404).build();
		}
		return ResponseEntity.of(Optional.of(result));
	}

	@PostMapping(path = "/courses", consumes = "application/json")
	public Course getCourse(@RequestBody Course course) {
		return this.courseService.addCourse(course);
	}

}
