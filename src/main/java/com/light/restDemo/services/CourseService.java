package com.light.restDemo.services;

import java.util.List;

import com.light.restDemo.entities.Course;

public interface CourseService {
	public List<Course> getCourses();
	public Course getCourse(long courseId);
	public Course addCourse(Course course);
}