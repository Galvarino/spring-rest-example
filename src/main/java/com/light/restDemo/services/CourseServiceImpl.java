package com.light.restDemo.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.light.restDemo.dao.CourseDao;
import com.light.restDemo.entities.Course;

@Service
public class CourseServiceImpl implements CourseService {

//	List<Course> list;
	@Autowired
	private CourseDao courseDao;

	public CourseServiceImpl() {
//		list = new ArrayList<>();
//		list.add(new Course(14, "Java Core", "Basics of Java"));
//		list.add(new Course(15, "Spring Boot Core", "Basics of Spring Boot"));
//		list.add(new Course(16, "Hibernate Course", "Basics of Hibernate"));
	}

	@Override
	public List<Course> getCourses() {
		// TODO Auto-generated method stub
//		return list;
		return courseDao.findAll();
	}

	@Override
	public Course getCourse(long courseId) {
		// TODO Auto-generated method stub
//		Course result = null;
//		for (Course c : list) {
//			if (c.getId() == courseId) {
//				result = c;
//				break;
//			}
//		}
//		return result;
		return courseDao.getReferenceById(courseId);
	}

	@Override
	public Course addCourse(Course course) {
		// TODO Auto-generated method stub
//		list.add(course);
//		System.out.println(list);
//		return course;
		return courseDao.save(course);
	}
}
